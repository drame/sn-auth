package sn.auth.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import sn.auth.entities.AppUser;
import sn.auth.inter.AppUserInterface;

@RestController
public class AppUserRest {
	@Autowired
	private AppUserInterface appUser;
	
	@PostMapping("/register")
	public Object register(@RequestBody AppUser user,BindingResult br) {
		return appUser.register(user, br);
	}
}
