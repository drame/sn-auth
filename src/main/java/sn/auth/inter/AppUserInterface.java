package sn.auth.inter;

import org.springframework.validation.BindingResult;

import sn.auth.entities.AppUser;

public interface AppUserInterface {
	public Object register(AppUser user,BindingResult br);
	public Object register(AppUser user);
	

}
