package sn.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.auth.entities.AppRole;

public interface AppRoleRepository extends JpaRepository<AppRole, Long>{

}
