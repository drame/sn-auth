package sn.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.auth.entities.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long>  {
public AppUser findByUsername(String username);
}
