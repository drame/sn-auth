package sn.auth.metier;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import sn.auth.entities.AppUser;
import sn.auth.inter.AppUserInterface;
import sn.auth.repository.AppUserRepository;

@Service
public class AppUserImpl implements AppUserInterface {
 @Autowired
 private AppUserRepository userRep;
  
 public Object register(@Valid AppUser user,BindingResult br) {
	 Map<String,String> m= new HashMap<>(); 
	 if(br.hasErrors()) {
		 for(FieldError fe:br.getFieldErrors()) {
			 m.put(fe.getField(), fe.getDefaultMessage());
		 }
		 return m;
	 }
	 BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
	 user.setPassword(bc.encode(user.getPassword()));
	 userRep.save(user);
	 return null;
 }

@Override
public Object register(AppUser user) {
	 BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
	 user.setPassword(bc.encode(user.getPassword()));
	 userRep.save(user);
	 return null;
}
	
}
