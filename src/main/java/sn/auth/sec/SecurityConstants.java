package sn.auth.sec;

public class SecurityConstants {
public static final String SECRET="ddrame@secret";
public static final long EXPIRATION_TIME=864_000_000;
public static final String TOKEn_PREFIX="Bearer ";
public static final String HEADER_STRING="Authorization";
}
