package sn.auth.sec;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import sn.auth.entities.AppRole;
import sn.auth.entities.AppUser;
import sn.auth.repository.AppUserRepository;
@Service
public class AppUserDetailsService implements  UserDetailsService {
    @Autowired
	private AppUserRepository appUserRepository;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser user= appUserRepository.findByUsername(username);
		if(user==null) {
			throw new UsernameNotFoundException("Aucun utilisateur trouvé !");
		}
		List<GrantedAuthority> authorities= new ArrayList<>();
		for(AppRole role: user.getRoles()) {
		authorities.add(new SimpleGrantedAuthority(role.getRolename()));
	    }
		User u= new User(username, user.getPassword(), authorities);
		return u;
	}
	

}
