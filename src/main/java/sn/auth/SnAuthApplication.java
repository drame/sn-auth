package sn.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import sn.auth.entities.AppRole;
import sn.auth.entities.AppUser;
import sn.auth.inter.AppUserInterface;
import sn.auth.repository.AppRoleRepository;
import sn.auth.repository.AppUserRepository;

@SpringBootApplication
public class SnAuthApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(SnAuthApplication.class, args);
	}
    
	@Autowired
	private AppUserInterface userRep;
	@Autowired
	private AppRoleRepository roleRep;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		AppRole r1= new AppRole("ADMIN");
		
		AppRole r2= new AppRole("CHEF_AGENT");
		AppRole r3= new AppRole("AGENT");
		
		r1=roleRep.save(r1);
		r2=roleRep.save(r2);
		r3=roleRep.save(r3);
		List<AppRole> adminRoles= new ArrayList<>();
		List<AppRole> chefAgentRoles= new ArrayList<>();
		List<AppRole> agentRoles= new ArrayList<>();
		
		adminRoles.add(r1);
		adminRoles.add(r2);
		adminRoles.add(r3);
		
	
		chefAgentRoles.add(r2);
		chefAgentRoles.add(r3);
		
		agentRoles.add(r3);
		
		AppUser u1= new AppUser("admin", "12345",adminRoles);
		AppUser u2= new AppUser("dieyne", "12345",chefAgentRoles);
		AppUser u3= new AppUser("drame", "12345",agentRoles);
		
		userRep.register(u1);
		userRep.register(u2);
		userRep.register(u3);
		
	}
}
